= Style and tricks

== Horizontal ellipsis (`…` and `...`)

Three dots (`...`) are automatically transformed into `…`. You can also put `…` directly in your mixins.

Capitalization is not automatic after `…` because it is not systematic:
[quote, Chicago Manual of Style 13.51]
____________________________________________________________________
The first word after an ellipsis is capitalized if it begins a new grammatical sentence.
____________________________________________________________________

== When assembling, knowing which elements are empty

When you list elements, it is sometimes (but very rarely) useful to be able to know which elements are empty and which are not. 

The list of the non empty elements:

* is sent to separators, when they are mixins and take an object parameter (see exemple)
* is made available in `listInfo.nonEmpty` in the `itemz > item` structure

++++
<script>
spawnEditor('en_US', 
`mixin asmMixin(params)
  if params && params.nonEmpty && params.nonEmpty.length==3
    | and also
  else
    | and
- let WITH_3 = true;
p
  itemz {mode:'single_sentence', separator: ',', last_separator: 'asmMixin'}
    item
      | first
    item
      | second
    item
      if WITH_3
        | third

`, 'irst, second and also third'
);
</script>
++++
will output either _first, second and also third_ or _first and second_ depending on `WITH_3` flag.

* When in a a `eachz` structure, elements are objects, while when in an `itemz > item` structure, elements are integers.
* When you read `listInfo.nonEmpty` or `params.nonEmpty`, it can be `undefined`: this happens when RosaeNLG is actually testing if the elements are empty or not. RosaeNLG will make a second call with `xxx.nonEmpty` properly populated. Thus just test and ignore if `undefined`.
