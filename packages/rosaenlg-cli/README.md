# RosaeNLG Command Line Interface

CLI interface for RosaeNLG.

For documentation, see [RosaeNLG documentation](https://rosaenlg.org).
