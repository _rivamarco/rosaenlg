const assert = require('assert');
const italianWords = require('../dist/words.json');

describe('italian-words-dict', function() {
  it('should work', function(done) {
    assert(italianWords != null);
    assert(Object.keys(italianWords).length > 100);
    const pizza = italianWords['pizza'];
    assert(pizza != null);
    assert.equal(pizza['G'], 'F');
    assert.equal(pizza['P'], 'pizze');
    done();
  });
});
