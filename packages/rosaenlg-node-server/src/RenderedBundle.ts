import { RenderOptions } from './RenderOptions';

export interface RenderedBundle {
  text: string;
  renderOptions: RenderOptions;
}
