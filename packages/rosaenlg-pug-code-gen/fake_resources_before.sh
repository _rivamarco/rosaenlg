# rename

# en
mv ../english-verbs-gerunds/dist/gerunds.json ../english-verbs-gerunds/dist/gerunds.json.BU
mv ../english-verbs-irregular/dist/verbs.json ../english-verbs-irregular/dist/verbs.json.BU
mv ../english-plurals-list/dist/plurals.json ../english-plurals-list/dist/plurals.json.BU

# fr
mv ../french-words-gender-lefff/dist/words.json ../french-words-gender-lefff/dist/words.json.BU
mv ../french-verbs-lefff/dist/conjugations.json ../french-verbs-lefff/dist/conjugations.json.BU

# de
mv ../german-words-dict/dist/words.json ../german-words-dict/dist/words.json.BU
mv ../german-adjectives-dict/dist/adjectives.json ../german-adjectives-dict/dist/adjectives.json.BU
mv ../german-verbs-dict/dist/verbs.json ../german-verbs-dict/dist/verbs.json.BU

# it
mv ../italian-words-dict/dist/words.json ../italian-words-dict/dist/words.json.BU
mv ../italian-adjectives-dict/dist/adjectives.json ../italian-adjectives-dict/dist/adjectives.json.BU
mv ../italian-verbs-dict/dist/verbs.json ../italian-verbs-dict/dist/verbs.json.BU

# fake ones
# en
echo {} > ../english-verbs-gerunds/dist/gerunds.json
echo {} > ../english-verbs-irregular/dist/verbs.json
echo {} > ../english-plurals-list/dist/plurals.json
# fr
echo {} > ../french-words-gender-lefff/dist/words.json
echo {} > ../french-verbs-lefff/dist/conjugations.json
# de
echo {} > ../german-words-dict/dist/words.json
echo {} > ../german-adjectives-dict/dist/adjectives.json
echo {} > ../german-verbs-dict/dist/verbs.json
# it
echo {} > ../italian-words-dict/dist/words.json
echo {} > ../italian-adjectives-dict/dist/adjectives.json
echo {} > ../italian-verbs-dict/dist/verbs.json
